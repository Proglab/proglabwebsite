// This is where project configuration and plugin options are located. 
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Proglab',
  plugins: [
    {
      use: '@gridsome/source-contentful',
      options: {
        space: 'o9u4eb8iqhjq', // required
        accessToken: 'mqYuOPz4utrFLjU48a4tvf740bHk9SHMGulo-ZnoQJc', // required
        host: 'cdn.contentful.com',
        environment: 'master',
        typeName: 'Contentful'
      }
    }
  ]
}
